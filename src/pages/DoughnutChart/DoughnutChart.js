import { Doughnut } from 'vue-chartjs'
export default {
  extends: Doughnut,
  mounted: function mounted () {
    this.renderChart({
      labels: ['15-20', '21-30', '31-40', 'above 40'],
      datasets: [{
        backgroundColor: ['#41B883', '#E46651', '#00D8FF', '#DD1B16'],
        data: [40, 20, 80, 10]
      }]
    }, {
      responsive: true,
      maintainAspectRatio: false
    })
  }
}
