import { Line } from 'vue-chartjs'

export default {
  extends: Line,
  mounted () {
    this.renderChart({
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Des'],
      datasets: [
        { label: 'new user', backgroundColor: '#41B883', data: [40, 30, 50, 80, 50, 40, 60, 50, 60, 90, 100, 120] }
      ]
    }, { responsive: true, maintainAspectRatio: false })
  }
}
