import Vue from 'vue'
import Router from 'vue-router'

import MyLayout from 'layouts/MyLayout.vue'
import Index from 'pages/Index.vue'
import User from 'pages/User.vue'
import Login from 'pages/Login.vue'

Vue.use(Router)

const routes = [
  {
    path: '/',
    component: MyLayout,
    children: [
      { path: '', name: 'Index', component: Index },
      { path: '/user', name: 'User', component: User }
    ]
  },
  { path: '/login', name: 'Login', component: Login }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
